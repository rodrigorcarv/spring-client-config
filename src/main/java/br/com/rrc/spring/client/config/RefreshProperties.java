package br.com.rrc.spring.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
@RefreshScope
public class RefreshProperties {

    @Value("${cliente.name}")
    private String clientName;

    public String getClientName() {
        return clientName;
    }
}
