package br.com.rrc.spring.client.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
public class SpringClientConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringClientConfigApplication.class, args);
	}

}
