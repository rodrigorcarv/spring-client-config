package br.com.rrc.spring.client.config.rest;

import br.com.rrc.spring.client.config.RefreshProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/app")
public class ConfigTest {

    @Autowired
    private RefreshProperties refreshProperties;

    @RequestMapping(value = "/client", method = RequestMethod.GET)
    public String getClientName() {
        return refreshProperties.getClientName();
    }
}
